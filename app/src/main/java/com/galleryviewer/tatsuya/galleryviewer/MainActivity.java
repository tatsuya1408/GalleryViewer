package com.galleryviewer.tatsuya.galleryviewer;

import com.galleryviewer.tatsuya.galleryviewer.adapter.GridViewAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {
    private GridView imageGridView;
    private GridViewAdapter gridViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    private void initComponents() {

        final int[] listImgID = new int[]{
                R.drawable.img1, R.drawable.img2, R.drawable.img3
        };

        this.imageGridView = super.findViewById(R.id.imageGridView);
        this.gridViewAdapter = new GridViewAdapter(this, listImgID);
        this.imageGridView.setAdapter(this.gridViewAdapter);

        final Intent intent = new Intent(MainActivity.this, ViewDetailActivity.class);
        intent.putExtra("listImgID", listImgID);
        this.imageGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                intent.putExtra("image", listImgID[position]);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }
}

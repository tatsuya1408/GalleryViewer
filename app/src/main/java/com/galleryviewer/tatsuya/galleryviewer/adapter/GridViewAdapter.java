package com.galleryviewer.tatsuya.galleryviewer.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GridViewAdapter extends BaseAdapter {
    private Context context;
    private int[] listImgID;

    public GridViewAdapter(Context context, int[] listImgID) {
        this.context = context;
        this.listImgID = listImgID;
    }

    @Override
    public int getCount() {
        return this.listImgID.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(new GridView.LayoutParams(400, 400));
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(this.listImgID[position]);
        return imageView;
    }
}

package com.galleryviewer.tatsuya.galleryviewer;

import com.galleryviewer.tatsuya.galleryviewer.adapter.GalleryAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;

public class ViewDetailActivity extends AppCompatActivity {
    private Gallery gallery;
    private ImageView currentImageView;
    private int[] listImgID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detail);
        Intent intent = getIntent();
        this.listImgID = intent.getIntArrayExtra("listImgID");
        gallery = (Gallery) findViewById(R.id.simpleGallery);
        this.currentImageView = (ImageView) findViewById(R.id.iv_gridview);
        this.currentImageView.setImageResource(intent.getIntExtra("image", 0));

        GalleryAdapter galleryApdater = new GalleryAdapter(super.getApplicationContext(), this.listImgID);
        this.gallery.setAdapter(galleryApdater);
        this.gallery.setSpacing(10);
        this.gallery.setSelection(intent.getIntExtra("position", 0));
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentImageView.setImageResource(listImgID[position]);
            }
        });
    }
}

package com.galleryviewer.tatsuya.galleryviewer.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class GalleryAdapter extends BaseAdapter {
    private Context context;
    private int[] listImgID;

    public GalleryAdapter(Context context, int[] listImgID) {
        this.context = context;
        this.listImgID = listImgID;
    }

    @Override
    public int getCount() {
        return this.listImgID.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(this.context);
        imageView.setImageResource(this.listImgID[position]);
        imageView.setLayoutParams(new Gallery.LayoutParams(200,200));
        return imageView;
    }
}
